import unittest
from automobile import decoupage
from automobile import switch
from automobile import changementFormatDate
class TestAutomobile(unittest.TestCase):
    def testChangementFormatDate(self):
        self.assertEqual(changementFormatDate("2019-03-25"),"25/03/2019")
    def testDecoupage(self):
        self.assertEqual(decoupage("un, deux, trois"),['un','deux','trois'])
        self.assertEqual(decoupage("quatre, cinq, six"),['quatre','cinq','six'])
    def testswitch(self):
        donnee_test = switch()
        self.assertEqual(donnee_test[19],"3822 Omar Square Suite 257 Port Emily, OK 43251")
        self.assertEqual(donnee_test[20],"Smith")
        self.assertEqual(donnee_test[21],"Jerome")
        self.assertEqual(donnee_test[22],"OVC-568")
        self.assertEqual(donnee_test[23],"03/05/2012")
        self.assertEqual(donnee_test[24],"9780082351764")
        self.assertEqual(donnee_test[25],"Williams Inc")
        self.assertEqual(donnee_test[26],"Enhanced well-modulated moderator")
        self.assertEqual(donnee_test[27],"LightGoldenRodYellow")
        self.assertEqual(donnee_test[28],"45-1743376")
        self.assertEqual(donnee_test[29],"34-7904216")
        self.assertEqual(donnee_test[30],"3462")
        self.assertEqual(donnee_test[31],"37578077")
        self.assertEqual(donnee_test[32],"32")
        self.assertEqual(donnee_test[33],"3827")
        self.assertEqual(donnee_test[34],"110")
        self.assertEqual(donnee_test[35],"Inc")
        self.assertEqual(donnee_test[36],"92-3625175")
        self.assertEqual(donnee_test[37],"79266482")