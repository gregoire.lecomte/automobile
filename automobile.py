import csv
import datetime
def changementFormatDate(YYYY_MM_DD):
    DD_MM_YYYY = datetime.datetime.strptime(YYYY_MM_DD, '%Y-%m-%d')
    DD_MM_YYYY = DD_MM_YYYY.strftime('%d/%m/%Y')
    return DD_MM_YYYY
def decoupage(phrase_a_decouper):
    type_voiture,variante_voiture,version_voiture = phrase_a_decouper.split(', ')
    type_variante_version = [type_voiture,variante_voiture,version_voiture]
    return type_variante_version
def switch():
    row_ech = ["adresse_titulaire","nom","prenom", "immatriculation", "date_immatriculation" ,"vin", "marque" ,"denomination_commerciale", "couleur", "carrosserie" ,"categorie", "cylindree" ,"energie" ,"places" ,"poids", "puissance" ,"type" ,"variante" ,"version"]
    donnee = []
    with open('auto.csv','r') as csvfile:
        reader = csv.reader(csvfile, delimiter='|')
        for row in reader:
            if reader.line_num !=1:
                row_ech = []
                for nbr in [0,11,8,9,5,16,10,6,3,1,2,4,7,12,13,14,15]:
                    row_ech.append(row[nbr])                
                for var in decoupage(row_ech.pop(16)):
                    row_ech.append(var)
                row_ech[4] = changementFormatDate(row_ech[4])
            i = 0
            while i != len(row_ech):
                donnee.append(row_ech[i])
                i = i + 1
    return donnee
def ecriture(donnee,delimiteur):
    with open('fichier.csv','w') as csvfile:
        writer = csv.writer(csvfile ,delimiter=delimiteur)
        nbr = 0
        while nbr != len(donnee):
            i = 0
            auto = []
            while i != 19:
                auto.append(donnee.pop(0))
                i += 1
            writer.writerow(auto)
        nbr += 1 

ecriture(switch(),";") 